package com.example.demo.human;

import com.sun.istack.NotNull;
import org.springframework.lang.NonNullApi;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table
public class Human {
    @Id
    @GeneratedValue
    @Column
    @NotNull
    private UUID id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private Integer age;

    @Column
    @Lob
    private String description;

    public Human() {}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
