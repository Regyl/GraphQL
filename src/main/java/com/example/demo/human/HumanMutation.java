package com.example.demo.human;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HumanMutation implements GraphQLMutationResolver {
    @Autowired
    HumanService service;

    public Human createHuman(String name, Integer age, String description) {
        Human human = new Human();
        human.setAge(age);
        human.setName(name);
        human.setDescription(description);
        return service.save(human);
    }
}
