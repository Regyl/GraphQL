package com.example.demo.human;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class HumanQuery implements GraphQLQueryResolver {
    @Autowired
    HumanService service;


    public List<Human> getHumans() {
        return service.findAll();
    }

    public Human getHuman(UUID id) {
        return service.findById(id);
    }
}
