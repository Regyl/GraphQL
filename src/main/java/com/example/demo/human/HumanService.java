package com.example.demo.human;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class HumanService {
    final HumanRepository repository;
    public HumanService(HumanRepository repository) {
        this.repository=repository;
    }

    @Transactional(readOnly = true)
    public List<Human> findAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Human findById(UUID id) {
        return repository.findById(id).get();
    }

    @Transactional
    public Human save(Human human) {
        return repository.save(human);
    }
}
